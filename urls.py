from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Home Page
    url(r'^$', 'personalsite.apps.home.views.home', name='homepage'),

    # Contato
    url(r'^contato/$', 'personalsite.apps.home.views.contato', name='contato'),

    # Quicklinks
    url(r'^links/$', 'personalsite.apps.quicklinks.views.links', name='links'),

    # Projetos
    url(r'^projetos/$',
        'personalsite.apps.projetos.views.lista',
        name='projetos'),
    url(r'^projeto/(?P<slug>[-\w+]+)/$',
        'personalsite.apps.projetos.views.detalhes',
        name='projeto'),

    # Admin
    (r'^admin/', include(admin.site.urls)),
    (r'^grappelli/', include('grappelli.urls')),
    (r'^admin/filebrowser/', include('filebrowser.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    )

    urlpatterns += staticfiles_urlpatterns()
