# Personal Site

Esse projeto foi feito para o meu site, para a divulgação do meu portfólio e os meus dados profissionais. Ele contêm as seguintes funcionalidades:

- Cadastro de Projetos;
- Cadastro de Perfis;
- Cadastro de Quicklinks;
- Formulário de Contato;
- Briefing (Breve);
- Blog (Breve).

Licença: BSD

