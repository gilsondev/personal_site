# -*- coding: utf8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext

from models import Projeto


def lista(request):
    """Lista todos os
    projetos desenvolvidos.
    """
    projetos = Projeto.objects.all()

    return render_to_response('projetos/projetos_list.html',
        {'projetos': projetos}, context_instance=RequestContext(request))


def detalhes(request, slug):
    """Consulta os detalhes do projeto
    selecionado
    """
    projeto = Projeto.objects.get(slug=slug)

    return render_to_response('projetos/projetos_detail.html',
        {'projeto': projeto}, context_instance=RequestContext(request))