# -*- coding: utf8 -*-

from django.contrib import admin

from models import Projeto


class ProjetoAdmin(admin.ModelAdmin):
    search_fields = ('nome', 'tecnologias')
    list_display = ('nome', 'endereco', 'tecnologias')
    prepopulated_fields = {'slug': ('nome',)}

admin.site.register(Projeto, ProjetoAdmin)