# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Projeto.screenshot'
        db.add_column('projetos', 'screenshot', self.gf('filebrowser.fields.FileBrowseField')(default='', max_length=200), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Projeto.screenshot'
        db.delete_column('projetos', 'screenshot')


    models = {
        'projetos.projeto': {
            'Meta': {'object_name': 'Projeto', 'db_table': "'projetos'"},
            'descricao': ('django.db.models.fields.TextField', [], {}),
            'endereco': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'screenshot': ('filebrowser.fields.FileBrowseField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '60', 'db_index': 'True'}),
            'tecnologias': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['projetos']
