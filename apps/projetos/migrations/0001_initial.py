# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Projeto'
        db.create_table('projetos', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=60, db_index=True)),
            ('endereco', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('tecnologias', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('descricao', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('projetos', ['Projeto'])


    def backwards(self, orm):
        
        # Deleting model 'Projeto'
        db.delete_table('projetos')


    models = {
        'projetos.projeto': {
            'Meta': {'object_name': 'Projeto', 'db_table': "'projetos'"},
            'descricao': ('django.db.models.fields.TextField', [], {}),
            'endereco': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '60', 'db_index': 'True'}),
            'tecnologias': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['projetos']
