# -*- coding: utf8 -*-

from django.test import TestCase

from personalsite.apps.projetos.models import Projeto


class ProjetoTest(TestCase):
    """Efetuando testes unitários para
    a funcionalidade de cadastrar projetos.
    """

    def test_inserir_projetos(self):
        """O projeto e inserido?"""
        projeto = Projeto.objects.create(
            nome="Projeto Teste",
            slug="projeto-teste",
            endereco="http://www.enderecoprojeto.com.br",
            tecnologias="HTML5, CSS3, Django, Apache",
            descricao="""Aqui estara armazenado a descricao
            do projeto. Essa descricao vai mostrar de forma simples
            e direta o objetivo, e o publico alvo do projeto.
            """
        )

        self.assertTrue(projeto.id)