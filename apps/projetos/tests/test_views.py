# -*- coding: utf8 -*-

from django.test import TestCase
from django.core.urlresolvers import reverse


class ProjetosViewsTest(TestCase):
    """Efetua os testes na camada
    de visualização da funcionalidade
    """

    def test_projetos_sucesso(self):
        """ Foi acessado a pagina ?"""
        response = self.client.get(reverse('projetos'))
        self.assertEquals(200, response.status_code)
        self.assertTemplateUsed(response, 'projetos/projetos_list.html')

    def test_detalhes_projeto(self):
        """ Foi exibido os detalhes do projeto?"""
        response = self.client.get('/projeto/click-samambaia')
        self.assertEquals(200, response.status_code)
        self.assertTemplateUsed(response, 'projetos/projetos_detail.html')