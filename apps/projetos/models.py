# -*- coding: utf8 -*-

from django.db import models
from django.db.models import permalink
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import smart_unicode

from filebrowser.fields import FileBrowseField


class Projeto(models.Model):
    """Classe responsável por
    encapsulsar os dados dos projetos.
    """
    nome = models.CharField(_(u'Nome do Projeto'), max_length=60)
    slug = models.SlugField(_(u'URL do Projeto'), max_length=60)
    endereco = models.URLField(_(u'Endereço do Projeto'), blank=True)
    imagem_box = FileBrowseField(
        _(u'Imagem do Box'),
        max_length=200,
        directory='screenshots/')
    screenshot = FileBrowseField(
        _(u'Screenshot do Projeto'),
        max_length=200,
        directory='screenshots/')
    tecnologias = models.CharField(_(u'Tecnologias Usadas'), max_length=120)
    descricao = models.TextField(_(u'Descrição do Projeto'))

    class Meta:
        verbose_name = 'Projeto'
        verbose_name_plural = 'Projetos'
        db_table = 'projetos'

    def __unicode__(self):
        return smart_unicode(self.nome)

    def get_absolute_url(self):
        return ('personalsite.apps.projetos.views.detalhes', (),
            {'slug': self.slug})
    get_absolute_url = permalink(get_absolute_url)
