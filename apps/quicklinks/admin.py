# -*- coding: utf8 -*-

from django.contrib import admin

from personalsite.apps.quicklinks.models import Link, ClassificacaoLink


class LinkAdmin(admin.ModelAdmin):
    """Registro o modelo de dados dos links
    no Django Admin
    """
    search_fields = ('titulo',)
    list_display = ('titulo', 'classificacao', 'url',)


class ClassificacaoLinkAdmin(admin.ModelAdmin):
    """Registrando o modelo de dados da classificação
    dos links no Django Admin
    """
    search_fields = ('classificacao',)
    list_display = ('classificacao',)

admin.site.register(Link, LinkAdmin)
admin.site.register(ClassificacaoLink, ClassificacaoLinkAdmin)