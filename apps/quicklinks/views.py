# -*- coding: utf8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext

from personalsite.apps.quicklinks.models import Link


def links(request):
    """Lista os links
    favoritados
    """
    links = Link.objects.all()

    return render_to_response('links/list_links.html', {'links': links},
        context_instance=RequestContext(request))
