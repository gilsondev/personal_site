# -*- coding: utf8 -*-

from django.test import TestCase

from personalsite.apps.quicklinks.models import Link, ClassificacaoLink

class LinkTest(TestCase):
    """Efetua testes unitários
    com o cadastro de links no
    site.
    """
    fixtures = ['classificacao.json']

    def setUp(self):
        self.classificacao = ClassificacaoLink.objects.get(pk=1)

    def test_inserir_link(self):
        """O link e inserido?"""
        link = Link.objects.create(
            classificacao=self.classificacao,
            titulo="Titulo do Link",
            url="http://gilsondev.com"
        )

        self.assertTrue(link.id)

    def test_inserir_classificacao_link(self):
        """A classificacao do link foi inserida?"""
        classificacao = ClassificacaoLink.objects.create(
            classificacao="Ferramentas"
        )

        self.assertTrue(classificacao.id)