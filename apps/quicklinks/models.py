# -*- coding: utf8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class ClassificacaoLink(models.Model):
    """Classe responsável por classificar
    os links cadastrados
    """
    classificacao = models.CharField(_(u'Classificação dos Links'),
        max_length=40)

    class Meta:
        db_table = 'classificacao_links'
        verbose_name = u'Classificação do Link'
        verbose_name_plural = u'Classificações dos Links'

    def __unicode__(self):
        return self.classificacao


class Link(models.Model):
    """Classe responsável por
    encapsular os dados dos links.
    """
    classificacao = models.ForeignKey(ClassificacaoLink,
        related_name='tipo_link')   
    titulo = models.CharField(_(u'Título do Link'), max_length=50)
    url = models.URLField(_(u'Endereço'))

    class Meta:
        db_table = 'links'
        verbose_name = 'Link'
        verbose_name_plural = 'Links'

    def __unicode__(self):
        return self.titulo