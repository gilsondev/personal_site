# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'ClassificacaoLink'
        db.create_table('classificacao_links', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('classificacao', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('quicklinks', ['ClassificacaoLink'])

        # Adding model 'Link'
        db.create_table('links', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('classificacao', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['quicklinks.ClassificacaoLink'])),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal('quicklinks', ['Link'])


    def backwards(self, orm):
        
        # Deleting model 'ClassificacaoLink'
        db.delete_table('classificacao_links')

        # Deleting model 'Link'
        db.delete_table('links')


    models = {
        'quicklinks.classificacaolink': {
            'Meta': {'object_name': 'ClassificacaoLink', 'db_table': "'classificacao_links'"},
            'classificacao': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'quicklinks.link': {
            'Meta': {'object_name': 'Link', 'db_table': "'links'"},
            'classificacao': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['quicklinks.ClassificacaoLink']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['quicklinks']
