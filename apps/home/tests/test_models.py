# -*- coding: utf8 -*-

from django.test import TestCase

from personalsite.apps.home.models import Perfil, Link


class PerfilTest(TestCase):
    """Efetua testes unitários
    com o conteúdo ``Sobre`` da
    página inicial
    """

    def test_inserir_perfil(self):
        """O perfil e inserido?"""
        perfil = Perfil.objects.create(
            descricao=u"""
            Trabalha com desenvolvimento web há 3 anos, 
            e usando as tecnologias como Python/Django e Java/VRaptor. 
            Entusiasta em Software Livre e Metodologias Ágeis, gosta de usar 
            ferramentas que ajuda no desenvolvimento de seus projetos.
            """,
            ativo=True
        )

        self.assertTrue(perfil.id)
