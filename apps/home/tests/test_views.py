# -*- coding: utf8 -*-

from django.test import TestCase
from django.core.urlresolvers import reverse


class HomeTest(TestCase):
    """Efetua testes na camada
    de visualização, ou seja, a
    home page do site.
    """
    def setUp(self):
        self.browser = Browser()

    def test_homepage_success(self):
        response = self.client.get(reverse('homepage'))
        self.assertEquals(200, response.status_code)
        self.assertTemplateUsed(response, 'home/index.html')

    def test_menu_footer(self):
        pass
