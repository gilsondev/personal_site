# -*- coding: utf8 -*-

import utils

from django.shortcuts import render_to_response
from django.template import RequestContext

from models import Perfil
from personalsite.apps.projetos.models import Projeto
from forms import FaleConoscoForm


def home(request):
    """Captura o perfil cadastrado
    e renderiza na página.
    """
    perfil = Perfil.objects.get(ativo=True)

    projetos = Projeto.objects.order_by('?')[:3]

    return render_to_response('home/index.html',
        {'perfil': perfil, 'projetos': projetos},
        context_instance=RequestContext(request))


def contato(request):
    """Invoca o formulário de contato
    como também envia o e-mail para o
    autor do site.
    """
    if request.method == 'POST':

        form = FaleConoscoForm(request.POST)

        if form.is_valid():
            nome = form.cleaned_data['nome']
            email = form.cleaned_data['email']
            mensagem = form.cleaned_data['mensagem']

            # Enviando a mensagem
            utils.enviar_contato(nome, email, mensagem)
            return render_to_response('home/sucesso_contato.html', {},
                context_instance=RequestContext(request))
    else:
        form = FaleConoscoForm()

    return render_to_response('home/form_contato.html', {'form': form},
        context_instance=RequestContext(request))
