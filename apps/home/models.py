# -*- coding: utf8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import smart_unicode


class Perfil(models.Model):
    """Classe responsável por
    encapsular os dados do perfil
    do autor do site.
    """
    descricao = models.TextField(_(u'Descrição do Autor'))
    ativo = models.BooleanField(_(u'Habilitar Visualização'), default=True)

    class Meta:
        db_table = 'perfil'
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfis'

    def __unicode__(self):
        return smart_unicode(self.descricao[:40])
