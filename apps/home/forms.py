# -*- coding: utf8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _


class FaleConoscoForm(forms.Form):
    """Formulário responsável por
    enviar um e-mail de contato para
    o autor do site.
    """
    nome = forms.CharField(label=_(u'Seu Nome'))
    email = forms.EmailField(label=_(u'Seu E-mail'))
    mensagem = forms.CharField(label=_(u'Mensagem'), widget=forms.Textarea)