# -*- coding: utf8 -*-

from django.contrib import admin

from personalsite.apps.home.models import Perfil


class PerfilAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'ativo')

admin.site.register(Perfil, PerfilAdmin)