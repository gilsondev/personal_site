# -*- coding: utf8 -*-

from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings


def enviar_contato(nome, email, mensagem):
    """Envia o assunto
    prenchido no fale conosco
    """
    assunto = "Mensagem do Site Gilson #DEV"
    mensagem = render_to_string(
        'home/email.txt',
        {'nome': nome, 'email': email, 'mensagem': mensagem}
    )

    send_mail(
        subject=assunto,
        message=mensagem,
        from_email=email,
        recipient_list=[settings.DEFAULT_FROM_EMAIL, ]
    )
